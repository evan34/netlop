function onLoad(event){
    if (this.status === 200){
        myXml = this.responseXML;
        readData(myXml);
    } else{
        console.log("Status de la reponse: %d(%s)",this.status, this.statusText);
        }
    }
    
    function onLoadEnd(event){
        console.log("le transfert est terminé.");
    }
    
    function readData (myXml) {
        var racine = myXml.documentElement;
        var element = "";  
        getFils(racine);
    }
     
      function getFils(myXml){
        for(var i = 0; i<myXml.childNodes.length; i++){ 
          var element = myXml.childNodes[i];
          if(element.nodeType==1){
                for(var j = 0; j<element.childNodes.length; j++){
                    
                    var noeudenfant = element.childNodes[j];
                    console.log(noeudenfant);
                    if(noeudenfant.nodeType==1){
                    var monImg = noeudenfant.getElementsByTagName("url")[0].childNodes[0].nodeValue;
                    var monTitre = noeudenfant.getElementsByTagName("nom")[0].childNodes[0].nodeValue;
                    var monGenre = noeudenfant.getElementsByTagName("genre")[0].childNodes[0].nodeValue;
                    var monRealisateur = noeudenfant.getElementsByTagName("realisateur")[0].childNodes[0].nodeValue;
                    var maDateSortie = noeudenfant.getElementsByTagName("dateSortie")[0].childNodes[0].nodeValue;
                    var monResume = noeudenfant.getElementsByTagName("resume")[0].childNodes[0].nodeValue;
                    affiche(monImg,monTitre,monGenre,monRealisateur,maDateSortie,monResume);
                }
                }
           
            }
            
        }
    
      }   
      
    function affiche (monImg,monTitre,monGenre,monRealisateur,maDateSortie,monResume){
    
        var maposition = document.getElementById('corps');//je me positionne sur l element avec l id 'corps'.
        corps.setAttribute("style","background-color:red");// je change couleur arriere plan du body avec le setAttribute.
        var monelement = document.createElement("div");// j ai creer mon element div.
        monelement.innerHTML="<h1>NETSLIP<h1>";//dans mon element (div) je rajoute du texte.
        monelement.setAttribute("style","margin-left:500px");
        maposition.appendChild(monelement);//mon element (div) devient l enfant de ma position (body/corps).
    
        for (i=0; i<4; i++){//creation de ma boucle creeant mes 4 navs. 
            let nav = document.createElement("nav");
            nav.setAttribute("style","background-color:white");
            nav.setAttribute("style","display:inline");
            corps.appendChild(nav);//fusion a son parent corps.
       }
       
       let navnum = document.getElementsByTagName("nav");//transformation des 4 navs en un tableau permettant de les recuperer a l unite via l index.
       navnum[0].innerHTML =" " + "<strong>FILMS</strong> " + " " + " " ;
       navnum[1].innerHTML = " <strong>SERIES</strong> "+ " " + " " ;
       navnum[2].innerHTML = " <strong>DOCUS</strong> " + " " + " " ;
       navnum[3].innerHTML = " " + " <strong>MANGAS</strong>";
       
    
        let section= document.createElement("section");//je declare et creer la let (section) dans ma boucle.
        section.setAttribute("id","section");// je lui donne un id via le setAttribute.
        section.setAttribute("style","background-color:black");//ajout de la classe manga.
        document.body.appendChild(section);
    
        let sectionnum = document.getElementById("section");//transformer les sections en tableau.
    
        let article= document.createElement("article");//creation de l article, fils de section.
                article.innerHTML="<h4>"+monTitre+"<h4>";// ajout d un titre en h4.
                article.setAttribute("style","color:white");//ajout de style pour avoir ecriture blanche.
                sectionnum.appendChild(article);//fusion a l element parent sectionnum.
    
                let image1 = document.createElement("img");//cree element image, fils de article.
                        image1.setAttribute("src",monImg);//ajoute la source de l image. monImg sera la source grace a l appel de la fonction.
                        image1.setAttribute("alt","image films");
                        image1.setAttribute("width","25%");//ajustement de la taille.
                        image1.setAttribute("style","margin-left:450px");//creation d une marge.
                        article.appendChild(image1);//fusion avec element parent article.
    
                        let para=document.createElement("p");//creation paragraphe.
                                    para.innerHTML="Genre: " + monGenre + "<br>" + "Realisateur: " + monRealisateur + "<br>" + "Date de sortie: " + maDateSortie + "<br>" + "Resumé:<br><br>"+monResume;//ajout de tous les parametres de la fonction a afficher.
                                    para.setAttribute("style","color:white");
                                    para.setAttribute("margin-left","500px");
    
                                    article.appendChild(para); //fusion a son parent article.
    }
       
    const req = new XMLHttpRequest();//on instancie un nouvel objetXMLHttpRequest.
    //req.onProgress = onProgress;// methode qui affiche des erreurs s il y en a.
    //req.onerror = onError;//
    req.onload = onLoad;
    req.onloadend = onLoadEnd;// methode onloaded s affiche quand le telechargement est terminé.
    req.open('GET', 'http://netflop.bwb/films.xml', true);//chemin vers le fichier xml contenant les donnees.
    req.send(null);