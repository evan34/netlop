var express = require('express');
var router = express.Router();

// Récupération des modules du contrôleur
var film_controller = require('../controllers/controllerFilm');


router.get('/', film_controller.index);

router.get('/films', film_controller.film_list);

router.get('/film/create', film_controller.film_create_get);

router.post('/film/create', film_controller.film_create_post);

router.get('/film/:id/delete', film_controller.film_delete_get);

router.post('/film/:id/delete', film_controller.film_delete_post);

router.get('/film/:id/update', film_controller.film_update_get);

router.post('/film/:id/update', film_controller.film_update_post);

router.get('/film/:id', film_controller.film_detail);


module.exports = router;