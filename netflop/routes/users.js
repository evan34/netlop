var express = require('express');
var router = express.Router();
var controller_users = require('../controllers/controller_users');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/users/register', controller_users.users_register);

router.get('/users/login', controller_users.users_login);

module.exports = router;
