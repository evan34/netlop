//Affichages

let request = require('request');
let bodyParser = require('body-parser')


exports.film_list = function (req, res, next) {
    let id = req.param('search');

    request("http://www.omdbapi.com/?apikey=b16e06a3&t="+id, 
    { json: true }, (err, resReq, body) => {
    
    if(err) {
    console.log(err);
    }
    console.log(body)
    
    
        res.render('list_film', {films: body});
    
    
    });
    
    /*request("http://www.omdbapi.com/?apikey=b16e06a3&s="+id, 
    { json: true }, (err, resReq, body) => {
    
    if(err) {
    console.log(err);
    }
    console.log(JSON.stringify(body))
    
    
        res.render('list_film', JSON.stringify(body));
    
    
    });*/
};

exports.film_create_get = function (req, res, next) {
    res.render('create');
};

exports.film_create_post = function (req, res, next) {
    res.render('list_film');
};

exports.film_delete_get = function (req, res, next) {
    res.render('delete');
};

exports.film_delete_post = function (req, res, next) {
    res.render('list_film');
};

exports.film_update_get = function (req, res, next) {
    res.render('update');
};

exports.film_update_post = function (req, res, next) {
    res.render('list_film');
};

exports.film_detail = function (req, res, next) {
    let detail = req.params.id;
    console.log(detail)
    request("http://www.omdbapi.com/?apikey=b16e06a3&t="+detail, 
    { json: true }, (err, resReq, body) => {
    
    if(err) {
    console.log(err);
    }
    res.render('detail_film', {films:body});
    });
};

exports.index = function (req, res, next) {

};

