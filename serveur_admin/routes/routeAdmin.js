var express = require('express');
var router = express.Router();

var admin_controller = require('../controller/controllerAdmin');


router.get('/', admin_controller.login_connect);

router.post('/admin', admin_controller.admin_connect);

router.get('/register', admin_controller.register_connect);

router.post('/register_post', admin_controller.post_register_user);

router.get('/forgotPassword', admin_controller.forgot_connect);

router.get('/error', admin_controller.error_connect);

router.get('/admin/userManage', admin_controller.user_manage);

router.get('/admin/userDetails', admin_controller.user_Details);

router.post('/admin/userDetails', admin_controller.update_User);

router.get('/admin/userDelete', admin_controller.delete_User);

router.get('/admin/filmManage', admin_controller.film_manage);

router.post('/admin/filmSend', admin_controller.film_send);

//router.get('/admin/listFilms', admin_controller.list_film);

module.exports = router;